---
layout: project
title: RetroFlag Pi Case Safe Shutdown Script
date: 2018-10-02 20:12:56 +0000
icon: icon.gif
links:
  - title: Safe Shutdown Script
    description: Alternative shutdown script
    url: https://gitlab.com/unixispower/retroflag-picase
---
A replacement shutdown script for Retroflag's Raspberry Pi cases. The original
scripts by Retroflag have a messy installer and lack customization options so I
wrote my own version.
