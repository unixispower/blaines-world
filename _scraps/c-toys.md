---
layout: project
title: C Toys
date: 2023-06-24 03:32:02 +0000
icon: icon.gif
links:
  - title: Source
    description: Various oddities written in C
    url: https://gitlab.com/unixispower/c-toys
---
Some small C programs that do silly things. This is a playground to scratch that
itch that that comes with random ideas. Portability is not guaranteed.
