{%- comment -%}
    Find all documents in a collection with a specific list of categories

    Parameters:
        include.collection: name of collection to filter
        include.categories: list of categories a document must have to be included
        include.sort_by: field to sort by, defaults to date
        include.reverse: if true reverse order, defaults to true
    Output:
        filtered_collection: list of matching documents
{%- endcomment -%}


{%- assign _collection = site.collections
    | where: 'label', include.collection | first -%}
{%- assign _sort_by = include.sort_by | default: 'date' -%}

{%- if include.reverse == false -%}
    {%- assign _reverse = false -%}
{%- else -%}
    {%- assign _reverse = true -%}
{%- endif -%}

{%- assign _item_categories = include.categories | join: ',' | split: ',' -%}
{%- assign filtered_collection = _collection.docs
    | where_exp: '_document', '_document.categories == _item_categories'
    | sort: _sort_by -%}

{%- if _reverse -%}
    {%- assign filtered_collection = filtered_collection | reverse -%}
{%- endif -%}
