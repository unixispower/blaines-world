---
layout: project
title: Wasteland Radio
date: 2016-07-02 08:20:00 +0000
log_date: 2021-07-17 09:42:39 +0000
icon: icon.gif
flair: in-progress
links:
  - title: OS Setup Script
    description: Raspberry Pi software initialization script
    url: https://gitlab.com/unixispower/wasteland-radio
---
A 1940's Philco radio converted into a retrofuturistic internet media streamer.
The damaged condition of the radio inspired a Fallout themed project.
