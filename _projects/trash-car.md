---
layout: project
title: Trash Car
date: 2020-07-13 18:50:00 +0000
log_date: 2020-08-22 10:29:00 +0000
icon: icon.gif
flair: in-progress
---
A toy remote control car upgraded with hobby RC components and other
junk from my collection. I got it without the transmitter when I was a kid
and saved it for a rainy day. Almost 2 decades later I picked it back up.
