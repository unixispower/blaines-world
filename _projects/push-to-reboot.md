---
layout: project
title: Push to Reboot
date: 2018-04-13 21:37:00 +0000
log_date: 2018-05-24 09:04:00 +0000
icon: icon.gif
flair: dead
links:
  - title: Firmware Source
    description: (Incomplete) device firmware source code
    url: https://gitlab.com/unixispower/push-to-reboot
---
A power strip controllable via DTMF commands issued using a handheld radio.
My original goal was to provide a way to reset network equipment while away
from home.

Since starting the project I have cleaned up my network and no longer
experience device lockups. I gave it some thought and decided to defunct
the project as it requires legacy equipment to build and has timing attack
issues at the protocol-level.

I keep the project listed here because it can be used as a reasonable starting
point for other AVR GCC projects. It also contains a memory-conscious
implementation of HMAC-SHA1 that may be useful to someone.
