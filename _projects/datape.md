---
layout: project
title: DATAPE
date: 2017-10-29 18:20:00 +0000
log_date: 2017-11-12 06:48:00 +0000
icon: icon.gif
flair: in-progress
links:
  - title: Codec Utility
    description: Command line utility that encodes and decodes DATAPE audio
    url: https://gitlab.com/unixispower/datape
---
A data storage format that is intended to be reliable when used on aging
cassette tape hardware. This is mostly for fun, but somewhat research on
data storage techniques.
