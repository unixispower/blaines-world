---
layout: project
title: Super PAC 64
date: 2021-05-20 13:06:54 +0000
log_date: 2021-06-26 02:09:29 +0000
icon: icon.gif
flair: complete
cover: dolch-pac-64-front.jpg
---
Upgrades and experiments with the Dolch PAC 64 "Network Sniffer". This is _not_
just another gut-and-stuff project; almost all the original hardware was kept
in-place or upgraded to play nice with newer technology.

Here are some glamour shots of the finished box with the new hardware labeled.

{% include image.html filename="dolch-pac-64-front.jpg"
    alt="Dolch PAC 64 running Windows 98 SE" %}

{% include image.html filename="dolch-pac-64-side.jpg"
    alt="Side view of case showing new parts" %}

{% include image.html filename="dolch-pac-64-back.jpg"
    alt="Inside view of case showing new parts" %}


## Hardware

| Part          | Model                                    | Original |
|---------------|------------------------------------------|----------|
| Motherboard   | TMC AI5TV-1.3 (Intel 82430VX chipset)    | Yes      |
| Processor     | Intel Pentium P54 200MHz (Socket 5 296)  | Yes      |
| Memory        | 128MB (4x32MB) 72-pin EDO                | Some     |
| Flash Disk    | StarTech IDE2CF + Transcend CF200I 4GB   | No       |
| Floppy Drive  | Teac FD-235HF-8291-U5                    | Yes      |
| Optical Drive | Teac CD-316E-903-U                       | Yes      |
| Graphics Card | C&T 65548 PCI card with LCD controller   | Yes      |
| Display       | GoldStar LP121S1 12.1" LCD 800x600       | Yes      |
| Sound Card    | Sound Blaster Live! Value CT4830         | No       |
| Ethernet Card | Adaptec ANA-6511/UC 10/100 Fast Ethernet | Yes      |
| Wireless Card | Edimax EW-7128g 802.11g/b                | No       |


## Modifications

- Swapped failing HDD for IDE/CF adapter and industrial CF card
- Doubled RAM to maximum supported by motherboard
- Added PC card slot cover to expose existing USB and reset button
- Added wireless network adapter and sound card
- Added stereo speakers and amplifier to case


## TODO
I am done with the upgrades I had planned for the PAC, but there are some
minor things I would like to visit in the future.

- Investigate issues that intermittently prevent POSTing on cold boot. I have a
  hunch this is related to power supply capacitors. For now I can work around
  this by hitting the reset button just after powering on.
- Fix dead spot in the LCD brightness slider. This is pretty low priority since
  I only use the screen at maximum brightness.
- Remove yellowing from the LCD. I think this is due to the plastic polarizer
  layer turning yellow with age. It should be easy to find some polarizer
  film, but removing the old film could break the glass layer in the LCD.
- Add battery holder to Dallas RTC and add IC socket to the motherboard. The
  RTC that is installed in the PAC still has juice (as of June 2021) and had
  the correct time when I first powered it up. At this point I'm curious how
  long the original battery will hold out.
