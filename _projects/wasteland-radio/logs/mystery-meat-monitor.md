---
layout: document
title:  Mystery Meat Monitor
date: 2016-12-18 04:12:00 +0000
categories: [wasteland-radio, logs]
cover: carry-front.jpg
---
In order to use the Carry-I's monitor with a Raspberry Pi I needed to learn a 
little more about the beast. The most direct route would be to run a 
diagnostic program to discover the graphics hardware, but that isn't an 
option. I can't power on the computer because I lost the power supply brick at 
some point in the last couple years (oops). 

{% include image.html filename="carry-front.jpg" alt="Carry-I front view" %}

The computer is very small, only occupying a footprint the length and width of 
the monitor. It seems like the model would have made a good candidate for a 
POS or cataloging computer.

{% include image.html filename="carry-side.jpg" alt="Carry-I side view" %}

The 9 pin video connector is on the right side near the power connector. The 
label "display" isn't very descriptive; time to go inside.

{% include image.html filename="io-ports.jpg" alt="IO ports" %}

Two screws hold the outer casing to an inner frame. The outer casing slides 
off the rear of the computer after the screws are removed. Inside there are 
two 3.5" bays. The left bay holds a 40-pin ATA device; the right holds a 
34-pin floppy device.

{% include image.html filename="drive-bays.jpg" alt="Drive bays" %}

Four screws fix the inner chassis to the motherboard. The inner frame slides 
off of the front of the board after the screws are removed.

{% include image.html filename="motherboard.jpg" alt="Motherboard" %}

I looked at the area of the board near the video port for ICs that appeared to 
be connected to the port. After coming up empty on a few searches I found a 
chip that looked promising. The identifier TM6310 hit on an [archive of a 
4chan technology ... err, discussion](https://warosu.org/g/thread/55235351). 
The part seems to be a combination 
[MDA](https://en.wikipedia.org/wiki/IBM_Monochrome_Display_Adapter) /
[Hercules](https://en.wikipedia.org/wiki/Hercules_Graphics_Card) 
video driver and parallel port driver.

{% include image.html filename="video-ic.jpg" alt="Video IC" %}

One mystery solved, but more questions emerge. It looks like I'll be looking 
for a way to drive a monochrome display from the Raspberry Pi.
