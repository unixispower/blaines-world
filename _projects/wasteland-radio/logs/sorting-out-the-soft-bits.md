---
layout: document
title: Sorting Out the Soft Bits
date: 2020-05-14 16:15:00 +0000
categories: [wasteland-radio, logs]
cover: ncmpc-vanilla.jpg
---
The current global pandemic has me stuck inside with little to do so I
decided to focus my energy into the Jukebox project and knock out the bits I've
been putting off for too long. I previously pinned down most of the hardware to
be used in the project: a Raspberry Pi for the SBC, a CRT TV for the display,
a cheap audio amplifier and automotive speakers for sound... but didn't think
too far into the software part. I vaguely figured I'd use MPD and a CLI client
to access it, but now I've managed to work out exactly how to configure
everything.

The SBC used for driving the display and speakers of the jukebox is a 
[Raspberry Pi 3B+](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/).
There are many popular options for operating systems for the Pi, most of which 
focus on solving a particular task or running a specific software stack. For
this project I decided to start lightweight and build up by using [Alpine 
Linux](https://alpinelinux.org/). Alpine is geared toward small image size and 
minimal running environment making it a perfect choice for things like Docker
images and the Raspberry Pi. As a bonus it uses something called
["diskless mode"](https://wiki.alpinelinux.org/wiki/Installation#Installation_Handbook)
out of the box which causes the system to operate in RAM and only touch 
persistent storage when committing disk changes manually. This is fantastic
considering the number of SD cards I have nuked using IO-heavy images like
Raspbian.

I started with a vanilla Alpine image just to see how a CLI would look on the
mini TV. Right off the bat I noticed the entire display was fuzzy, colorized
text was pulsing in intensity, and any output near the edges of the screen was
cut off by the bezel around the tube.

{% include image.html filename="terminal-vanilla.jpg"
    alt="Linux terminal with vanilla configuration" %}

At least one of these issues was solved by using the Raspberry Pi's builtin
[config.txt](https://www.raspberrypi.org/documentation/configuration/config-txt/).
Alpine (and some other distros) are configured to load an additional file
called `usrconfig.txt` from the root of the boot media. I added `overscan_*`
options to eliminate the text cutoff and tried out `sdtv_disable_colourburst`
to improve crispness, but I didn't see any difference.

{% include image.html filename="terminal-tweaked.jpg"
    alt="Linux terminal with config.txt tweaks applied" %}

Even with all the text squeezed onto the screen it was still near impossible to
read at a distance. After doing a bit of research I found that the Linux
framebuffer console supports different fonts and that the Apine repo had
[Terminus](http://terminus-font.sourceforge.net/) console fonts available.
I tested quite a few variants out using the `setfont` utility and settled on
one called `ter-128b`. The larger font size helped reduce the fuzziness and
flashing effect quite a bit.

{% include image.html filename="terminal-terminus.jpg"
    alt="Linux terminal showing Terminus console font ter-128b" %}

With the display bits sorted it was time to make the music parts happen. To
play music I installed [MPD](https://www.musicpd.org/) to the Pi. MPD is a
basic server that plays music locally for connected clients. To control MPD
from locally I installed [ncmpc](https://musicpd.org/clients/ncmpc/) which has
a text interface _almost_ perfect for the jukebox. I only want a subset of the
features of ncmpc to be available, but this customization will be in another
log.

{% include image.html filename="ncmpc-vanilla.jpg"
    alt="ncmpc running on the Raspberry Pi" %}


All of the software was now installed, but not configured to start on boot.
By default Alpine will boot into an interactive TTY on the framebuffer console
at startup. To boot directly into ncmpc I edited `/etc/inittab` to start a
custom script on TTY1 instead of getty. The custom script simply sets the
framebuffer console font to Terminus and runs ncmpc as an unprivileged user
(link to the script is below).

As I set software up on the Pi I created an installer that initializes Alpine
for me (I screw things up often and like to have instructions/installers for
the inevitable case when I need to re-image). This installer and instructions
on how to use it are located in this
[Git repo](https://gitlab.com/unixispower/wasteland-jukebox).
