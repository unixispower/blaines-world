---
layout: document
title: System on Board
date: 2021-07-16 02:12:54 +0000
categories: [wasteland-radio, logs]
cover: radio-hardware-mounted.jpg
---
Since the last log a lot has changed with the radio hardware setup. I have been
working on developing software in a couple of different physical locations
which prompted me to make some changes to improve portability. The project has
also been renamed from "Wasteland Jukebox" to "Wasteland Radio" to better
reflect what functionality the device will have; old logs still refer to the
project as "Wasteland Jukebox".

I attached all the PCBs and power supplies to a thin wooden board so the
entire hardware setup could be removed from the radio as a single unit. An SPST
relay module has also been added to allow the Pi to control the power state of
the TV so the screen can be put to sleep after a short period of inactivity.
I used standoffs for the Pi and audio amp and zip-tied the relay module to a
cable mount because the mounting holes are very small and located too close to
the edge of the PCB for my comfort.

{% include image.html filename="radio-hardware-mounted.jpg"
    alt="Radio hardware mounted to a board" %}

The relay module is attached to board pin 3 (GPIO 2) and interrupts the
positive wire of the TV's power supply cable. The power supply is wired to the
relay so it is normally disconnected unless the relay is energized. Upon
startup the Pi drives the relay pin low to power on the screen when it
processes a [gpio directive in usercfg.txt](https://www.raspberrypi.org/documentation/configuration/config-txt/gpio.md).

{% include image.html filename="tv-power-relay.jpg"
    alt="Relay used to interrupt TV power supply" %}

While I was working on the software side of things I started to have issues
with the TV I am using. The image on screen would sometimes "compress"
horizontally and overlap itself causing the console text to be unreadable.

{% include image.html filename="distorted-tv-image.jpg"
    alt="TV image distorted due to hardware issue" %}

I accidentally discovered that the issue was related to a physical connection
when I bumped the monitor and it suddenly corrected itself -- for a few
seconds. I had a hunch that the issue was with the point-to-point wiring that
connected the tube to the main TV PCB so I replaced all the connecting wires
with new ones. When that didn't work I powered up the TV and started gently
touching each component on the main PCB with a pencil eraser until it became
obvious that the issue was with a specific capacitor. The capacitor in question
was perfectly fine, but the solder joint under it was dead cold from the
factory.

{% include image.html filename="floating-pin.jpg"
    alt="Closeup of TV PCB with floating capacitor pin highlighted" %}

After resoldering the floating pin the TV started to display its picture as
expected and no longer reacted to light taps and physical agitations. While I
was inside the TV I also removed the internal speaker and built-in antenna.
When the TV is plugged into the composite output of the Pi it emits a very
harsh tone unless the volume knob is turned all the way down. The simplest way
to prevent accidental noise was to simply cut out the speaker.

I have been developing a new software stack for the radio using this hardware
setup. Previously I was planning to use `ncmpc` as a front-end for `mpd`, but
I found it to be _too_ feature packed for the dead-simple style of control I am
looking for. I have since started to write my own `mpd` client that also
handles other housekeeping tasks like WiFi connection and screen sleep
management. I am nearing completion of the core features I have planned so my
next log will be dedicated to covering the improved software and operating
system configuration.
