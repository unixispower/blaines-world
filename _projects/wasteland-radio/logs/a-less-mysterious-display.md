---
layout: document
title: A Less Mysterious Display
date: 2017-06-22 00:58:00 +0000
categories: [wasteland-radio, logs]
cover: rca-exploded.jpg
---
My recent trip to the local thrift shop yielded a new display for the radio. I 
have decided not to use the MDA monitor and instead use a small RCA color 
TV/monitor. This is an easy decision since the monitor will accept the Pi's 
composite output without the need for an adapter.

{% include image.html filename="rca-front.jpg" alt="RCA TV front view" %}

I have never seen a color tube this small before. There is a speaker grille 
and a control panel mounted on the side of the unit.

{% include image.html filename="rca-side.jpg" alt="RCA TV side view" %}

On the back there are inputs for audio and video. I will only be using the 
video input as the radio cabinet already contains speakers.

{% include image.html filename="rca-back.jpg" alt="RCA TV back view" %}

The TV is powered by a 12V DC adapter or by a "D" cell battery sled.

{% include image.html filename="rca-bottom.jpg" alt="RCA TV bottom view" %}

The TV is too big to fit in the radio with the casing installed. The casing 
was a bit difficult to remove, but I managed to get past the outer shell that
with a couple pairs of vice grips.  

{% include image.html filename="rca-exploded.jpg" alt="RCA TV exploded" %}

Inside the casing there was the following label:

{% include image.html filename="hv-warning.jpg" alt="High voltage warning label" %}

I should probably find a good way to shield the tube and board once I get it 
installed in the cabinet. Hopefully something lightweight like aluminum or 
copper tape will do the trick.
