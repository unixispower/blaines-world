---
layout: document
title: All Cracked Up
date: 2017-10-12 03:44:00 +0000
categories: [tape-artchive, logs]
cover: cracked-gear.jpg
---
Today I received the package of assorted tape drive belts in the mail. None of 
the belts in the package were a direct fit for either set of pulleys so I 
trimmed the next largest sizes and spliced them with super glue.

{% include image.html filename="assorted-belts.jpg"
    alt="Assortment of square replacement belts" %}

The trimmed belts seemed to fit well, but there was still a very noticeable 
fast flutter present in playback. I searched for more causes of flutter and 
found that a poorly oiled capstan bearing may be to blame. I oiled the pinch 
roller and capstan bearing on the Sony with synthetic 0W-20 oil. I found that 
a lightweight motor oil was recommended on a couple 
[forum](http://www.tapeheads.net/showthread.php?t=11604) 
[posts](http://audiokarma.org/forums/index.php?threads/teac-a-7300-capstan-issues.715140/).

I didn't hear an improvement after oiling the bearings so I started to grease 
the plastic parts. I took apart enough of the mechanism to remove all the 
plastic pieces then cleaned them with alcohol. I got too heavy handed with one 
of the plastic gears and broke it during cleaning.

{% include image.html filename="cracked-gear.jpg" alt="Cracked plastic gear" %}

I made an attempt to glue the gear together with superglue, but I did so 
carelessly and ended up misaligning the pieces. I put the deck back together 
with the sloppy gear with poor results. The tape repeatedly jammed and got 
stuck in the roller.

I have ruined the Sony deck -- not beyond repair, but beyond care. The Sony 
TC-FX160 isn't a great deck to begin with. I simply don't wish to put anymore 
time or money into trying to get a $3 thrift shop find working.

I would switch to the Panasonic deck, but the transport on it needs work. I'm 
pretty tired of blindly hacking at cassette decks or I would give a shot at 
lubricating it. I'm still in search of a decent recorder for this project and 
long term use. Hopefully I'll have some luck junk shopping soon.
