---
layout: document
title: Space on Tape
date: 2017-10-24 06:11:00 +0000
categories: [tape-artchive, logs]
cover: solar-system-cover.png
---
Today I recorded and presented my first tape artchive (to a very small 
audience of 2 in my apartment). In the last couple days I worked on 
finishing up the make script and put together some
[demo content](https://gitlab.com/unixispower/tape-artchive). I didn't want to 
publish my friend's art, so I prepared a tour of the planets of our solar 
system.

{% include image.html filename="solar-system-cover.png"
    alt="\"Solar System\" tape artchive cover" %}

I sourced images from the [NASA Images](https://archive.org/details/nasa) 
collection on the Internet Archive. I also sourced facts about planets from 
the [NASA Solar System Exploration](https://solarsystem.nasa.gov/planets/) 
pages. I don't make a very good voice actor, so I used 
[Festival](http://festvox.org/festival/) to generate speech for the slides. I 
recorded the artchive over an old self-help tape I found in a large set while 
dumpster-diving.

In previous tests with the Walkman I noticed playback speed could vary 
depending on the orientation of the tape player. When the player was on its 
side the tape made a scraping noise in its shell and slowed playback just 
enough to affect picture quality. I trimmed the length of the tape to 10 
minutes to reduce the drag of the tape in the shell.

{% include image.html filename="trimmed-tape.jpg" alt="Trimmed cassette tape" %}

Since testing I also found the speed adjustment pot on the Walkman. I used the 
test tape as a calibration tool to set the Walkman's playback speed. You can 
see in the captured image where colors are separated pre-adjustment, and where 
the colors align post-adjustment.  

{% include image.html filename="adjustment-test.png" alt="Adjustment test" %}

After adjusting the Walkman's speed the higher resolution modes looked a lot 
better. I am still going to use the Robot 36 mode because it is resilient to 
minor speed differences, and has a very short transmission time compared to 
other modes.  

Once I film the solar system demonstration tape I can wrap up the public 
portion of this project and get my friend's art compiled. Stay tuned for a 
demonstration soon.
