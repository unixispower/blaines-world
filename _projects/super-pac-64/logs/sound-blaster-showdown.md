---
layout: document
title: Sound Blaster Showdown
date: 2021-06-14 11:21:41 +0000
categories: [super-pac-64, logs]
cover: sbl-card-collection.jpg
---
While looking for upgrades for the PAC I came across a whole slew of Sound
Blaster Live! cards in a spare hardware bin. I found 5 cards in total; all the
cards have different model numbers with the exception of 2 cards that share the
same model number but have different board layouts. The models include
`SB0410` (24-bit), `SB0200` (Dell OEM), `CT4830` (Value), and two `CT4780`
(Value).

{% include image.html filename="sbl-card-collection.jpg"
    alt="Collection of assorted Sound Blaster Live! cards" %}

My requirements for a sound card are simple enough. Mainly I need a card that
will work well in Windows 98 SE and possibly has a game port to have some fun
with down the road. Luckily, the Sound Blaster Live! cards have good 98 support
and most of the models here have a game port.

The first card I decided to eliminate was the Sound Blaster Live! 24-bit model
`SB0410`. This card is a different size and shape than the other cards and
lacks a game port. The main audio IC is a Creative `CA0106-DAT LF` which is
also different than all the other Sound Blaster models I have on hand. This
appears to be a nice card, but it lacks the game port that the other cards
have.

{% include image.html filename="sbl-24bit-sb0410.jpg"
    alt="Sound Blaster Live! 24-bit card (model SB0410)" %}

Another card I decided to eliminate was the Sound Blaster Live! model `SB0200`.
This card looks simmilar to the other Sound Blaster Live! cards, but carries
the IC `EMU10K1X-DBQ` that is noticeably smaller than the ICs on the other
boards. Apparently this version of the card was sold by Dell as part of
prebuilt systems and lacks some features of the original and Value variants.

{% include image.html filename="sbl-dell-sb0200.jpg"
    alt="Sound Blaster Live! Dell OEM card (model SB0200)" %}

From what I can tell the remaining 3 cards are more or less equivalent. Two
of the cards are model number `CT4780` and the other is model `CT4830`. With
no clear winner I hit up [vogons.org](https://www.vogons.org/) to see if either
of the models had obvious issues. When looking for driver recommendations I
noticed several instances where one person would have luck with a certain
driver and model of card, but another person trying the same combo would have
issues. It appears that Creative might have used multiple versions of hardware
for models that have the same model number. I can confirm that at least the
same model cards can use different PCBs as seen on my two `CT4780` cards.

{% include image.html filename="sbl-value-ct4780a.jpg"
    alt="Sound Blaster Live! Value card (model CT4780)" %}

The cards have the same model number, but have different PCBs. One version of
the card has a couple additional headers that the other does not.

{% include image.html filename="sbl-value-ct4780b.jpg"
    alt="Sound Blaster Live! Value card (model CT4780)" %}

When searching for the final model `CT4830` I ran into an interesting
thread about [using Audigy2 ZS drivers with Sound Blaster Live! cards](https://www.vogons.org/viewtopic.php?f=62&t=74361).
Apparently this method is compatible with quite a few Sound Blaster Live! cards
and provides some benefits over the Live! VxD drivers like supporting the use
of custom soundfonts when running DOS programs under Windows.

{% include image.html filename="sbl-value-ct4830.jpg"
    alt="Sound Blaster Live! Value card (model CT4830)" %}

I decided to try out the Audigy driver method and go with the `CT4830` card
(other users had luck using the same model). After physically installing the
card I booted into Windows and followed the driver install guide. Several
reboots later I had working sound through headphones plugged into the green
port.

Initially I tried a known working card to make life easier, but after a
seamless install I got curious and decided to test all the cards with the
Audigy drivers. The Vogons guide by Joseph_Joestar specifically mentions
starting with a Windows install devoid of audio drivers so I broke out a second
CF card and found a pre-Sound-Blaster backup image I made. For each of the
remaining sound cards I wrote the "clean" 98 SE image to a CF card using `dd`,
booted up, and followed the install guide. Below is a summary of the Audigy
driver support.

| Card                         | Model  | Audio IC      | Audigy Support      |
|------------------------------|--------|---------------|---------------------|
| Sound Blaster Live! 24-bit   | SB0410 | CA0106-DAT LF | No                  |
| Sound Blaster Live! Dell OEM | SB0200 | EMU10K1X-DBQ  | No                  |
| Sound Blaster Live! Value    | CT4830 | EMU10K1-NEF   | Yes                 |
| Sound Blaster Live! Value    | CT4780 | EMU10K1-SFF   | Yes                 |
| Sound Blaster Live! Value    | CT4780 | EMU10K1-SEF   | Yes                 |

All drivers marked "Yes" above were detected by the Audigy installer. I didn't
do thorough tests of each card; I just installed drivers and ran `doom95`
within Windows to verify the default sound font was working. In the end I
picked the `CT4830`, but any of the Value series cards would have worked.

Overall I'm happy with the audio solution I ended up with. Even the tiny
soundfont included with the Audigy drivers sounds quite nice. A possible future
upgrade might be to add a small speaker to the case or connect the internal PC
speaker to the Sound Blaster.
