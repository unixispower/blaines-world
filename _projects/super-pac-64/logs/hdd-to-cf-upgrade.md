---
layout: document
title: HDD to CF Upgrade
date: 2021-05-20 13:06:54 +0000
categories: [super-pac-64, logs]
cover: cf-adapter-installed.jpg
---
Recently my employer has been clearing out old hardware and equipment that has
gone unused for quite some time. A now fairly _anchient_ Dolch PAC 64 was among
the long list of items that almost made the chopping block. Fortunately, I
managed to convince one of my overlords that the box was at least worth keeping
around for me to play with and freshen up my skills with legacy equipment.

{% include image.html filename="dolch-pac-64.jpg" alt="Dolch PAC 64" %}
    
When asking around about the machine I learned that it was deemed "broken"
around 15 years ago and never sent off for repair due to an extremely high
repair cost. Aside from its brokenness the unit is pristine and shows very
little wear.

I decided to power it up and have a look at the issues myself. To my surprise 
the unit powered right up and even retained the CMOS and RTC settings. Less
surprising was the error message telling me no boot device was detected. I
messed with some BIOS settings and re-detected the HDD and managed to get the
box to boot to Windows 95. After playing around some more I went to reboot the
box but got the dreaded disk error again. Easy fix I thought -- just a bad hard
drive that needs to be replaced.

It has been a while since I've messed with IDE devices, but I do remember
replacing a spinning disk with a compact flash card back when such tech was
more prevalent. After a quick search I found a popular
[CF/IDE adapter by StarTech](https://www.startech.com/en-us/hdd/ide2cf) and
hit up eBay for some
[4GB Transcend CF200I industrial CF cards](https://www.transcend-info.com/Embedded/Products/No-521).
I also picked up a
[4-pin to floppy cable adapter](https://www.amazon.com/gp/product/B07KS5SDZN/)
so the adapter could be powered by the connector freed up by removing the
original HDD.

{% include image.html filename="replacement-hardware.jpg"
    alt="CF/IDE adapter and industrial CF card ready to be installed" %}

A short wait later all the parts arrived and I got to work tearing the PAC 64
apart. The interior was easy to get to by removing some screws on the back of
the case and pulling the back half off like a lid. Under the plastic casing
was a metal frame that bolts together to form an electrical shield around
the internals. Removing this layer exposed a standard PC motherboard sporting a
Pentium processor and an abundance of PCI and ISA slots.

{% include image.html filename="pac-open-back.jpg"
    alt="PAC 64 with back cover removed" %}

Further buried in another metal casing was the CD drive, floppy drive, and the
hard drive that needed to be replaced. I removed this casing and unbolted the
drive from it. Even though it was failing I still wanted a backup of the
original image on it so I could try to restore it later.

The original disk is a 3.5" 2GB Quantum Fireball ST. I hooked the drive up to
my USB IDE adapter and tossed it on my main Linux box to image it with dd. My
first attempt was to run `dd if=/dev/sdb of=./image conv=sync,noerror` to pull
what data I could and fill the errors with zeroes. After transferring about
1.5GB dd froze and became unresponsive to Ctrl+C. I let the process sit for
about an hour but eventually killed it because the output file stopped growing
long before that point.

{% include image.html filename="original-hdd-recovery.jpg"
    alt="Original HDD attached to a USB-IDE adapter" %}

My second attempt used `ddrescue /dev/sdb hdimage mapfile` but again the disk
stopped giving data after about 1.5GB was transferred. I noticed this time
around that the disk emitted a harsh "screech" just before locking up. I waited
another hour for any console output to change, but no values advanced at all.
At this point I tried killing ddrescue with Ctrl+C but the program was entirely
unresponsive just like dd was. After doing some internet searching I found
another person that had a similar issue that was related to their USB IDE
adapter. I tried the same solution that worked for them -- power cycling the
drive and restarting ddrescue in direct access mode to copy the bits missed by
the first attempt: `ddrescue -d /dev/sdb hdimage mapfile`.

This advanced the ddrescue percent from 75% to about 76% then froze hard again
with no console output updates. I tried simply replugging the USB adapter and
found this had the same effect as power cycling the disk (but is probably less
harmful than spinning up and down over and over). Slowly I was able to build up
the rescued image a few percent at a time until the disk decided to cooperate
and give me almost all of the rest of the data in one go (screeching happily
every few seconds for about half an hour). At this point I had 99.99% of data
recovered and made one last attempt to grab the few unrecovered sectors with
`ddrescue -d -r3 /dev/sdb hdimage mapfile` but even after many hours I didn't
manage to get any more.

Overall I was only unable to recover about 2048 _bytes_ of data which I'm
pretty pleased with for a drive that only boots about 50% of the time.
Admittedly there are probably better approaches than killing the transfer every
time IO locked up, but using this workaround I was able to work past the
troublesome areas of the disk and make a nearly complete backup image.

I used the backup image to populate the new CF card by running
`ddrescue -f hdimage /dev/sdb` and popped the card into the IDE adapter. The
placement of the original HDD makes it difficult to access without removing the
cage that the drives are in so I placed the adapter on top of the drive cage
to allow access by simply removing the back cover of the PAC. The original IDE
cable was a little too short to re-locate the CF adapter so I replaced it with
a longer spare I had on-hand.

{% include image.html filename="cf-adapter-installed.jpg"
    alt="CF-IDE adapter installed as a hard drive replacement" %}

The hard part was done (or so I thought). I rebooted and ran the BIOS utility
for detecting drives and that's where the fun ended. I was presented with 3
options for CHS values in the detection utility (Normal, Large, LBA), all of
which result in the machine hanging at "Verifying DMI Pool Data" which can
indicate some hardware isn't initializing properly. Since I was working with a
backup I decided to try a fresh install of DOS in case the issue was related to
a mismatch in geometry of the original and replacement disks.

{% include image.html filename="cf-chs-options.jpg"
    alt="CHS autodetection options in the PAC 64 BIOS" %}

I verified the correct CHS values from the
[datasheet of the CF card I'm using](https://cdn.transcend-info.com/products/images/modelpic/701/No3147_20150916_Datasheet_CF220I%20v1.2.pdf)
which matched the "Normal" autodetect option so I tried that setting first.
For a easy test I decided to install DOS 6.22 and see if I could manage to get
that to boot. I found some
[6.22 install floppy images](https://winworldpc.com/download/c38fc38d-68c2-bbe2-80a6-4b11c3a4c2ac)
and wrote them with my Sony USB floppy drive using dd:
`dd if=DiskN.img of=/dev/sdb bs=1024 conv=sync && sync`.
The PAC 64 booted the first floppy and allowed me to run through a full install
with the new disk detected. After the install I rebooted and got the dreaded
"Non-system disk or disk error". I searched around for some quick fixes and
found that several sources recommended trying to rewrite the MBR using
`fdisk /MBR`. I booted back up to the first DOS install floppy and ran fdisk
but I was still unable to boot into the installed system afterwards. I thought
the issue could be related to the mode the disk was in, so I repeated a fresh
install using "LBA", "Normal", and "Large" mode options that were presented by
the BIOS. Every attempt resulted in the same disk error and a non-booting
system.

{% include image.html filename="dmi-pool-hang.jpg"
    alt="DMI pool hang during POST" %}

As a sanity check I tried to install Windows 95 from CD. Upon hitting the eject
button on the CD drive nothing happened. No lights, no sounds, nothing. I took
the PAC back apart and sure enough the IDE connector for the CD drive was
partially pulled out from where I wrangled the CF reader and cable into place.
I reconnected the drive and rebooted right into the installed DOS system from
earlier! The cause of some of my storage boot issues was a hardware issue all
along. I (re)learned a valuable lesson to check all hardware before going down
the software rabbit hole.

I made another attempt at booting the original image now that the hardware was
sorted. I restoring the image to the CF card using my host machine
`ddrescue -f hdimage /dev/sdb` and moved it to the PAC to boot, but ended up
with the same issue as the first attempt where the system hangs at "Verifying
DMI Pool Data". I also tried fiddling with the CHS setting and selecting all
detected "valid" modes as well as manually setting the values of the original
disk, but the image written to the CF card just wouldn't boot.

At this point I did some research on my DMI pool "error". I don't actually
receive an error so it seems that the BIOS is either stuck at a.) the DMI
verification stage or b.) just after the DMI stage when the MBR is being
executed (in this case the DMI data has been verified and there is simply no
output by the MBR/bootloader to indicate an error state). I also did some
research about how the Windows MBR boots the installed system and learned that
some files within the boot partition need to be
[arranged in a specific way within the FAT](https://en.wikipedia.org/wiki/IO.SYS#Disk_layout_requirements).

I had previously tried rewriting the MBR with `fdisk /MBR` but I did nothing to
correct the position of the key boot files after restoring the backup. When
searching for the DMI error [I found a thread](https://www.computerhope.com/forum/index.php?topic=21059.0)
with someone having the same issue and a few proposed solutions. One answer
advised booting from floppy and running `SYS C:` to copy the key files to the
correct positions within the boot partition.

The [SYS Command](https://en.wikipedia.org/wiki/SYS_(command)) places essential
boot files in the correct FAT positions but is tied tightly to the version of
DOS (Windows) it's modifying. I decided to play it safe and make a boot floppy
from the original hard drive since the OS on the machine has been customized
by an OEM (and because I don't know which version of Windows 95 it's running). I
plugged the original drive in one last time and booted up to Windows to create
a boot floppy. After it finished writing I manually copied `C:\DOS\SYS.COM` to
the root of the floppy and placed a copy of `C:\IO.SYS`, `C:\MSDOS.SYS`, and
`C:\COMMAND.COM` in a directory called `SYS` on the floppy. Finally I shut down
the system and replaced the original drive with the CF card once again.

With the boot floppy prepared I rebooted to it and got a DOS prompt. I ran
`SYS A:\SYS C:` and immediately received the error "Incorrect DOS version".
Weird. I booted into the existing system to create a boot disk to avoid this
exact scenario. Since the boot floppy version of DOS didn't work I decided to
go with one that had a bundled `SYS.COM` and use it to reinstall the files
already present on the original hard drive.

I used the DOS 6.22 install floppy images I found to create a recovery disk to
reinstall the boot files. I wrote a copy of the first DOS disk and deleted
`SCANDISK.EXE` and `QBASIC.EXE` to make room for the `SYS` folder I created
earlier. I booted up the PAC with the floppy and ran `SYS A:\SYS C:` only to
be repeatedly prompted to insert a system disk. It appears that simply copying
the boot files to a directory isn't good enough to make `SYS.COM` recognize
them for copying (or I might have copied the wrong collection of files --
finding documentation that details the inner workings of `SYS.COM` has proven
difficult).

As a last resort I ran `SYS C:` which installed the boot files included on the
DOS 6.22 boot disk instead of the ones I pulled from Windows 95. I rebooted and
got further in the process but couldn't move past an error during the Windows
boot that said the version of DOS I'm running was too old to start Windows. I
tried reflashing the CF card and repeating the `SYS` command with the C drive
as the source and the A drive as the target in an attempt to create a backup of
the existing boot files. I received an error that stated the C drive didn't
contain the boot files at all even though I see the files when browsing the
card on my Linux box. While I didn't get a fully booting system, I did manage
to move past the DMI pool hang point and make it into the Windows boot process.
It seems like the issue lies within the initial boot files; I just need to find
a good way to re-install those files to test that theory.

After many hours of troubleshooting I gave up on attempting to restore the
original disk image and accepted the DOS 6.22 install as confirmation that the
CF card is working. I intend to install Windows 98 on this computer anyway, so
a fully working restore of the original software is just a bonus. I might feel
compelled to take another crack at it in the future, but as of now I've spanned
more than 4 days trying to restore a "simple" backup and I'm out of patience.
If you are reading this and have ideas of what I'm doing wrong, please let me
know. I would love to make the original OS boot from a newer medium even if
it's just to prove that it can be done.
