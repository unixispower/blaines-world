---
layout: document
title: Rocking Out in Stereo
date: 2021-06-24 08:59:08 +0000
categories: [super-pac-64, logs]
cover: speaker-kit.jpg
---
After installing Windows 98 on the PAC I was disappointed at the lack of noises
coming from the box. There is a built-in PC speaker, but Windows does not use
this to play sounds. To solve the problem I installed a set of speakers to be
driven by the Sound Blaster Live! card that was just installed. The speakers
are nothing special; I pulled them from a TV when recycling it a few months
back.

{% include image.html filename="unmodified-speakers.jpg"
    alt="Speakers harvested from an LCD TV" %}

To drive the small speakers I picked up a
[PAM8406 based amplifier](http://www.mdfly.com/products/pam8406-5w-5w-stereo-amplifier-board-3-5mm-jack-screw-terminal-knob-pot.html).
I found this board after searching around for stereo amplifiers for small
speakers. This particular board can be set to run the PAM8406 in class AB mode
which should have less RF emissions compared to
[class D mode](https://en.wikipedia.org/wiki/Class-D_amplifier#Electromagnetic_interference).

{% include image.html filename="unmodified-amplifier.jpg"
    alt="PAM8406 stereo amplifier module" %}

I removed the existing mode resistor and soldered a small piece of wire to set
the module to use AB mode. I also replaced the power screw terminals with a
4-pin connector so the module could be connected directly to the PAC's power
supply. The existing wires on the speakers were very short so I replaced them
and terminated the ends of the new wires with ferrules.

The easiest way to connect the sound card to the amplifier was to use the rear
output of the card to drive the amplifier input. I added a 1/8" audio jack to
the card slot cover that had the USB breakouts and wired the jack to a pigtail
on the inside of the case. I also added a reset button to the same slot cover
because it fit perfectly in the remaining space. 

{% include image.html filename="speaker-kit.jpg"
    alt="Speakers, amplifier, and card slot cover ready to be installed" %}

I tried many orientations of the speakers and ended up removing the plastic
shroud pieces to make them smaller. After much debating I settled on mounting
the speakers to the supports that attach the inner frame of the PAC to the
outer plastic casing.

{% include image.html filename="speakers-installed.jpg"
    alt="Speakers installed on PAC 64 chassis" %}

I used a few pieces of double-sided foam tape to mount each speaker. A
mechanical connection with a solid adapter might be better, but foam tape has
the advantage of dampening vibrations from the speakers.

{% include image.html filename="right-speaker-installed.jpg"
    alt="Right speaker installed with double-sided foam tape" %}

The amplifier board fit nicely next to the CF-IDE adapter board. I installed
it the same way as the other board using several layers of double-sided foam
tape. The board has a volume knob, but the maximum volume seems to be the best
setting for the speakers I have.

{% include image.html filename="amplifier-installed.jpg"
    alt="Amplifier board installed inside the PAC" %}

I added a short patch cable to connect the sound card output to the amplifier
input. I plan to make this look a bit nicer by replacing the cable with one
that has right-angle connectors.

{% include image.html filename="audio-patch-cable.jpg"
    alt="Audio cable connecting the sound card to the amplifier" %}

The speakers sit just below holes that help hold the keyboard on. The audio
quality is about what you would expect from a small TV: very little bass and
a very clear high end.

{% include image.html filename="speaker-location.jpg"
    alt="Holes just above the installed speakers" %}

Once I had everything installed I booted into Windows and was greeted with that
sweet 98 startup sound. Initially speakers seemed like a minor addition, but
being able to hear the PAC without constantly switching headphones from one box
to another is really convenient.
