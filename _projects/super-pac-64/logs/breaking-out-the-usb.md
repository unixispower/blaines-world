---
layout: document
title: Breaking Out the USB
date: 2021-06-05 02:47:47 +0000
categories: [super-pac-64, logs]
cover: usb-card-slot-cable.jpg
---
Previously I mentioned I wanted to add a USB card to the PAC for mass storage.
Yesterday while taking a closer look at the motherboard manual I noticed the
board already has USB! There are no USB ports on the AI5TV, but there is a
non-standard USB header that can with a modified USB 2.0 header cable.

{% include image.html filename="ai5tv-usb-pinout.png"
    alt="AI5TV manual scan showing the pinout of the USB header" %}

Unlike the common USB header, the PAC is keyed in a way that removes the 5V pin
from one port. The PAC's version of the header is also _upside down_ when
compared to a standard USB header. Luckily a standard header cable can still be
plugged into the AI5TV with minimal modification.

{% include image.html filename="usb-card-slot-cable.jpg"
    alt="Standard USB 2.0 header cable with card slot mount" %}

A standard USB 2.0 header cable will fit on the motherboard, but won't
provide power to both ports without a minor wiring change. I cut both red
wires, stripped the insulator on all ends, and soldered all 4 wires into a
single point. After the solder cooled I unclipped the wires from the plug and
removed them to place heat shrink tubing over the joint.

{% include image.html filename="usb-cable-modified.jpg"
    alt="Standard USB 2.0 header cable modified to use the AI5TV USB header" %}

The USB header on the motherboard is located under the power supply in the PAC.
To get access to the header I had to remove the power supply and loosen the
drive cage screws. I inserted the plug on the cable into the header on the
motherboard with the unconnected pins oriented below pin 4 of the board header.

{% include image.html filename="usb-cable-installed.jpg"
    alt="Modified USB header cable installed on AI5TV USB header" %}

There wasn't a gap between the power supply and case to route the USB cable
so I ended up tucking it between the bottom edge of the PCI cards and the
motherboard.

{% include image.html filename="usb-cable-routing.jpg"
    alt="Modified USB header cable installed on AI5TV USB header" %}

I reassembled the PAC and booted to Windows for a test. Windows 98 does not have
a USB mass storage driver out of the box, but I found several places that
recommended the [Maximus Decim USB drivers](https://msfn.org/board/topic/43605-maximus-decim-native-usb-drivers/page/47/?tab=comments#comment-1000009)
that should recognize almost any generic storage device. The links on the
original forum post were dead, but I found another copy on
[Phil's Computer Lab](https://www.philscomputerlab.com/windows-98-usb-storage-driver.html).
I installed the drivers and rebooted.

I searched around the device manager and didn't find any trace of a USB
controller or devices. On a hunch I rebooted into the BIOS and found settings
to enable the onboard USB controller. Upon rebooting back into Windows the
on-board USB was recognized and installed.

{% include image.html filename="usb-controller-device.png"
    alt="USB flash drive mounted in Windows 98" %}

Seeing a removable storage device with a whole gigabyte of space is a relief
after using a few more cumbersome methods to move "large" files to the PAC.
Don't get me wrong, I like using floppies, but after a dozen disk swaps even I
start to get a little impatient.

{% include image.html filename="usb-flash-drive.png"
    alt="USB flash drive mounted in Windows 98" %}

I still have one PCI slot to fill and a whole slew of Sound Blaster Live! cards
to try out. The next hardware upgrade will be a sound card and some better
integrated speakers.
