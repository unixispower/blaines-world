---
layout: document
title: Going Online with Windows 98
date: 2021-06-02 23:37:38 +0000
categories: [super-pac-64, logs]
cover: blaines-world-retrozilla.png
---
The previous log detailed the replacement of the internal IDE disk with an
industrial CF card, but ultimately no new operating system was installed. The
PAC 64 originally ran a customized version of Windows 95, but a newer OS is
desirable for playing around with early Windows software so I opted to install
Windows 98 Second Edition. The PAC I'm working with has a Pentium processor
running at 200MHz and 64MB of RAM which puts Windows XP a little out of reach
(and I quite prefer 98 SE over ME so that's where I landed).

I started fresh by deleting the existing parition table off the CF card using
my Linux host and set the PAC's BIOS to use LBA mode with the card. I
downloaded [Windows 98 Second Edition (Retail Full)](https://winworldpc.com/product/windows-98/98-second-edition)
from WinWorld and burnt it to a CD. I also grabbed a copy of the
[Windows 98 Boot Floppy](https://winworldpc.com/product/microsoft-windows-boot-disk/98-se)
because the PAC 64 seems to lack the ability to boot from CD. I booted up with
the floppy and CD inserted, performed the initial disk format with "Large Disk
Mode" enabled, and rebooted into the Windows 98 installer.

{% include image.html filename="windows-98se-install.jpg"
    alt="Windows 98 Second Edition installer" %}

The download page on WinWorld lists a few "tested and verified working" serials
for the 98 SE downloads, but I was unable to get any of the ones listed working
with the "Retail Full" version of the installer. I lurked around and found a
[Reddit thread](https://www.reddit.com/r/windows98/comments/6trdvf/working_windows_98_se_retail_full_product_key/)
with someone having the same issue and had luck using the same key they did.
I progressed further into the installer and chose to do a "custom" install so
I could enable all the extras possible -- might as well make use of that
spacious 4GB disk.

After installing Windows the computer booted right into it without any issues.
The first thing I noticed was the lack of a startup sound, so I opened the
device manager to investigate. It turns out the PAC has a PC speaker, but no
dedicated sound card which explains the lack of startup sound. I researched
into [using the PC speaker to play system sounds](https://remember.the-aero.org/speaker/index2.htm),
but one such solution using `speak.exe` essentially locks the computer during
playback which is less than optimal for a responsive feeling system.

{% include image.html filename="mystery-pci-device.png"
    alt="Mysterious PCI device entry" %}

Also shown in the device manager were a couple of devices with issues including
an APM device and a mystery PCI card. The unknown PCI device listed in Windows
is a large and mysterious card with lots of ICs and a single connector. I don't
know what the card does and I don't have any hardware that goes with it so I
removed it hoping to have one less thing to worry about.

{% include image.html filename="mystery-pci-card.jpg"
    alt="Mysterious PCI card" %}

While I had the PAC apart I decided to explore a bit. With the PSU and drive
cage removed I was able identify motherboard as the model `AI5TV-1.306`. I did
some searching and found [a page](https://stason.org/TULARC/pc/motherboards/T/TMC-RESEARCH-CORPORATION-Pentium-AI5TV-VER-1-3C.html)
that lists the particular model as a "TMC Research Corporation" board. I
searched a little more and found [a scan of a manual](https://archive.org/details/manualzilla-id-5981770)
for what seems to be the same model board.

{% include image.html filename="ai5tv-motherboard.jpg"
    alt="AI5TV motherboard inside PAC 64" %}

While exploring the innards of the PAC I noticed the motherboard takes 2
different memory types. The manual confirms there are 4 SIMM sockets for EDO
and 1 DIMM socket for SDRAM. The board supports a maximum of 128MB EDO or 64MB
SDRAM to be installed. I don't have any EDO on hand, but I would quite like to
upgrade the RAM capacity in a future log.

The main reason I took the PAC apart wasn't for exploration, but for expansion.
After looking for ways to get online I found some recommendations for PCI WiFi
cards that support modern encryption. In the list of cards was the Edimax
EW-7128g which supports WPA2 and Windows 98 out of the box (sortof). Another
plus with the Edimax is the availability of
[official drivers](https://www.edimax.com/edimax/download/download/data/edimax/us/download/home_legacy_wireless_adapters/ew-7128g)
still hosted on the Edimax site.

{% include image.html filename="edimax-ew-7128g.jpg"
    alt="Edimax EW-7128g PCI WiFi card" %}

I installed the Edimax card in the PCI slot left open from removing the mystery
card and pieced the PAC back together. After getting all the hardware in place
I booted into the BIOS and enabled "Peer Concurrency" to allow the PCI bus to
control multiple cards at once. This is a tweak I noticed when skimming the
manual -- it should increase performance when using multiple PCI cards, but I
don't know if that's true in practice.

I booted up Windows and was delighted to see the card was recognized. Windows
prompted for a driver disk which is when I realized the official driver was 15
Megabytes! Not wanting to burn a CD for a file so small (and not wanting to
split the file across several floppies) I decided to move the driver across
using the existing wired network adapter. I connected an Ethernet cable from
the PAC to a specially configured WiFi router running
[ddwrt in client mode](https://wiki.dd-wrt.com/wiki/index.php/Client_Mode_Wireless).
The wireless router allows devices that are wired to it to communicate with a
specific WiFi network. This is more or less equivalent to wiring the PAC
directly to a wired network.

{% include image.html filename="router-ethernet-bridge.jpg"
    alt="Wireless router being used as a WiFi client" %}

Once I had the wired connection set up I opened `command.com` and tried to ping
a couple of sites without any luck. I noticed the "Connect to the Internet"
link on the Windows desktop so I ran it and selected the option to connect to
the internet via LAN and everything started to work as expected.

{% include image.html filename="internet-connection-wizard.png"
    alt="Internet connection wizard" %}

I downloaded the WiFi driver on my Linux host and ran `python3 -m http.server`
in the same directory to spin up a quick web server for the PAC to download
from. On the PAC I opened Internet Explorer, punched in the address of the
Linux host, and started the download of the driver without issues. Admittedly I
had one issue -- I tried downloading the `zip` of the driver at first before
realizing [native zip support didn't come to Windows until XP](https://devblogs.microsoft.com/oldnewthing/20180515-00/?p=98755).

I ran the driver exe and installed the official Edimax driver which required
inserting the 98 install CD to complete. Once everything was installed I
rebooted and ran the Edimax (Ralink) wireless utility only to hit a snag -- the
promised encryption modes were missing from the UI. During my initial research
I found a page confirming success with Ralink RT61 based cards and [a fix for
an issue where WPA2 is missing from the connection options](https://cloakedthargoid.wordpress.com/win9x-wpa2/).
The solution involves adding a profile for the WPA2 network but setting the
encryption type to one that _is_ available in the UI and then changing the
registry entry for the profile to manually specify the correct encryption type.

{% include image.html filename="ralink-wireless-utility.png"
    alt="Ralink Wireless Utility showing added profiles" %}

After hacking together a working WiFi profile I restarted the Edimax utility
and got a nice green icon confirming I was connected to the local wireless
access point. I opened up `command.com` to do another ping test, but got no
reply this time around. I looked up some network troubleshooting tips for
Windows 98 and found advice to check `winipcfg` to see if an IP was leased
properly. I ran the utility and found that both the Ethernet card and wireless
card had active IP leases so I manually released the Ethernet card lease which
allowed me to ping.

{% include image.html filename="ping-test-success.png"
    alt="Command prompt showing a ping test to Blaine's World" %}

Now that I was connected I started to worry a bit about security. I found
at least [one source](https://www.vogons.org/viewtopic.php?t=71509) that
confirmed my initial suspicion that a home router firewall was good enough to
protect against being scanned and fingerprinted by bots on _most_ networks. The
problem with my particular setup is that I'm not going to be using a "standard"
home network setup for access; this machine will be directly connected to the
internet without an external firewall on at least one network it uses. 

People securing these older boxes seems to be focused on 2 pieces of software:
antivirus and a personal firewall. Since it's probably going to be rare to run
into malware for Windows 98 just sitting out on the web I decided to worry
about the actual threat I have seen on the network I usually use -- port
scanners and bots probing at running services. I hit up some old software sites
and looked for a personal firewall and found a couple candidates to try.

Initially I selected a firewall based on reviews and screenshots. I ran across
Sygate Personal Firewall and found the graphs in the UI to just be plain neat.
I loaded up the [latest version listed on OldVersion.com](http://www.oldversion.com/windows/sygate-personal-firewall-5-6-2808)
but quickly found that it was too much for the PAC to handle and caused Windows
to lock up every few seconds. On top of the performance issues it caused the
wireless utility to disconnect often which was no good.

Next up I tried the ever-popular ZoneAlarm and found that it worked great out
of the box. OldVersion.com lists many new versions of ZoneAlarm, but after
searching a bit I decided on [version 5.5](http://www.oldversion.com/windows/zonealarm-5-5-094)
because it is [supposedly the last version that runs on Windows 9x](https://www.dell.com/community/Windows-General/zone-alarm-no-longer-supports-windows-98-and-ME/td-p/1899622).
To test if the firewall was working I ran a port scan on the PAC from my Linux
box using `nmap -Pn` and immediately recieved a notification from ZoneAlarm.
I looked in the log for ZoneAlarm and found that it blocked quite a bit of
external traffic in the short time I was performing my port scan test.

{% include image.html filename="zonealarm-pro-firewall.png"
    alt="ZoneAlarm Pro Firewall showing statistics" %}

The final piece of the web puzzle was the web browser. I don't expect much out
of a 200MHz Windows 98 box for browsing, but it would be nice to be able to
load up basic pages and even interact with some newer sites. I used Internet
Explorer to download files from my linux box to the PAC, but old IE is probably
my last choice for a sane browser (even when it was new). A quick search on
Vogons pointed me toward [RetroZilla](https://rn10950.github.io/RetroZillaWeb/)
which is a fork of Gecko with modern enhancements applied. I copied the
installer to the PAC using the python web server trick and loaded up [Blaine's
World](https://blaines.world/) as a test. At first my site appeared to load the
everything halted with an error message about "a security protocol not being
enabled". I searched the error and found a [RetroZilla issue explaining how to
enable the new SSL ciphers](https://github.com/rn10950/RetroZilla/issues/47)
which worked perfectly and allowed the site to load. Below is a screenshot of a
very broken Blaine's World running on the PAC. This site is built in HTML5
which is a different compatibility problem altogether, but at least it loads
and can even be considered _usable_ if you're patient enough.

{% include image.html filename="blaines-world-retrozilla.png"
    alt="Blaine's World shown in RetroZilla" %}

I noticed a couple of quirks while getting the wireless card set up. The
default mode of the Edimax utility seems to connect to any unsecured AP
automatically. The utility also seems to lack any concept of a "disconnected"
mode and only shows controls to activate different profiles. In order to force
the card to _not_ connect I had to create a dummy profile with WPA encryption
and a bogus SSID/password. This forces the card to attempt to connect to a
non-existent AP which prevents it from automatically connecting to the first
open AP it can find.

Another thing that isn't mentioned above is the transfer of files from the PAC
to the machine I'm writing this log entry on. The screenshots taken in this log
were pasted into `mspaint.exe` and saved as bitmaps then transferred to my
Linux box using [FileZilla 2.2.22](http://www.oldversion.com/windows/filezilla-2-2-22).
I originally tried the latest 2.x release of FileZilla (2.2.32), but that led
to a C++ runtime error which pointed me to a [useful post stating the last
supported version for Windows 98](https://forum.filezilla-project.org/viewtopic.php?t=2709).
On the Linux side I simply ran [VSFTPD](https://en.wikipedia.org/wiki/Vsftpd)
as a plain FTP server without encryption. Initially I tried using SFTP and SCP
via FileZilla and WinSCP, but neither program was able to create a secure
connection with my Linux box due to cipher suite incompatibilities. Perhaps a
future upgrade will be a USB card that will allow mass storage devices to be
used.
