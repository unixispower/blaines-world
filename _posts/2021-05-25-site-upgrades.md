---
layout: document
title: Site Upgrades
date: 2021-05-25 22:22:44 +0000
---
Long time, no talk. I've been making some changes around here the last few
days and figured I'd make a post about them. Perhaps the biggest usability
improvement has been the addition of
[navigation breadcrumbs](https://en.wikipedia.org/wiki/Breadcrumb_(navigation))!
Each page on the site now has a lovely trail of breadcrumbs at the top so you
can find your way back if you go deep and forget where you are.

The other major addition to the markup is [Open Graph tags](https://ogp.me/).
These `<meta>` tags inform instant messenger applications and social media
platforms about the content of the page when you share a link. Now when
sharing any page you should see a nice preview of the content and a pretty
cover image (if one is available).

If you are interested in the nitty-gritty details of how these are implemented,
just take a peek at the
[related](https://gitlab.com/unixispower/blaines-world/-/commit/0495b566559cd945cd3684c8a41f19f01a538c82)
[commits](https://gitlab.com/unixispower/blaines-world/-/commit/f467aa17102c591fe8c36df70eae3c3e5bbdc7a9)
on GitLab.
