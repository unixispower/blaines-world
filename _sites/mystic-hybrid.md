---
title: Mystic Hybrid
external_url: https://mystichybrid.info
order: 2
icon: icon.gif
---
Pixels, compression artifacts, and dithering, oh my. This is where I post photos
and digital art (I have Sony Mavicas and I don't know how to use them).
