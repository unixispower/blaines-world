---
layout: document
title: Privacy Policy
---
I run several websites as a hobby. These sites are hosted on Neocities, but also
contain code that utilize servers I have set up to provide additional
functionality (like hit counters and traffic analytics). This page outlines what
data is collected and how it is utilized for the following sites:

- [Blaine's World](https://blaines.world)
- [Mystic Hybrid](https://mystichybrid.info)
- [After the Beep](https://afterthebeep.tel)


## What is Collected
Requests for "static" resources like pages and images pass through servers
hosted by [Neocities](https://neocities.org/about). In order to provide basic
traffic statistics, Neocities collects visitor IP address and stores them for
a few hours before deleting them. More about this can be found under the
"Data Retention Policy" heading on the
[Neocities Legal FAQ and Contact page](https://neocities.org/legal).

Site statistics provided by Neocities are limited to counts of unique vistors
and page loads for a specific time period. This is useful for determining how
popular a site is, but does not provide other information like what site a
visitor has arrived from or what kind of browser they are using. Additional
analytics information is collected and stored on private servers I run. The
following data is collected by these servers:

- IP address of the visitor
- URL of page being visited
- Browser language
- Device screen dimensions
- [Page referrer](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referer)
- [User agent](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/User-Agent)

Uniquely identifying information like IP address is collected in server logs that
are [rotated](https://en.wikipedia.org/wiki/Log_rotation) daily and deleted
after 14 days. These logs exist to provide an audit trail when looking at
malicious access attempts from bots.

Visitor and browser information is also collected using an analytics tool called
[Umami](https://umami.is/). This tool generates statistics about visitors such
as what country the connection to the server was initiated from and what browser
software is being used. IP addresses are utilized by Umami, but not stored after
analysis. Data collected by Umami is retained in a database indefinitely.

### After the Beep
In addition to the above analytics, the name and phone number ("caller ID") of
the person calling is collected and stored. This information is used to detect
repeat callers and is never published publicly or shared with any party.


## How it is Used
The sites listed here are projects created for the entertainment of myself or
others. From time to time I will see an influx of traffic to specific sites or
pages which I want to know more about. The data that is collected helps me piece
together who is using these sites and what other sites out there are linking to
them.

**No collected data is sold or given to any party for any reason.**


## Contact
If you have any questions about data collection or wish for your data to be
scrubbed from my servers,
[please reach out via email](mailto:{{ site.data.site.email }}).
